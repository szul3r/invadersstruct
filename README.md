# InvadersStruct

Project for fun
The main goal is to gain knowledge about pygame module. To do that I write non-object code using the pygame module to create a Space Invaders clone (similar rules and mechanics).
Next step will be convert this project to object-oriented and provide improvements to code and game

![](ScreenShot.PNG)

"Icon made by Freepik from www.flaticon.com"
<a href="https://www.freepik.com/free-photos-vectors/background">Background vector created by freepik - www.freepik.com</a>