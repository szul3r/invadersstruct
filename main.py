import pygame
import logging, os, sys
import random
import math
from pygame import mixer

# ----for music----
# mixer.music.load("file-name.wav")
# mixer.music.play(-1)

if hasattr(sys, 'frozen'):
    font_family = "./freesansbold.ttf"
else:
    font_family = pygame.font.match_font("freesansbold.ttf")

logging.basicConfig(level=logging.DEBUG)
# Initializing the pygame module
pygame.init()

# Create screen (need do be tuple in parenthesis)
screen = pygame.display.set_mode((800, 600))
# Score text
score_value = 0
font = pygame.font.Font(font_family, 32)
score_x = 10
score_y = 10




def show_score(x, y):
    score = font.render("Score: " + str(score_value), True, (255, 25, 25))
    screen.blit(score, (x, y))


# Game Over text
game_over_text_string = "GAME OVER "
game_over_text_font = pygame.font.Font(font_family, 48)
game_over_text_x = 200
game_over_text_y = 250


def game_over_text(x, y):
    game_over_text_render = game_over_text_font.render(game_over_text_string + "Score: " + str(score_value), True, (255, 255, 255))
    screen.blit(game_over_text_render, (x, y))


background = pygame.image.load(r"IMG\space.png")

# Title and icon
pygame.display.set_caption("Invaders")
icon = pygame.image.load(r"IMG\space_invaders_alien.png")
pygame.display.set_icon(icon)

# Player
playerImg = pygame.image.load(r"IMG\space_invaders_player.png")
playerBulletImg = pygame.image.load(r"IMG\space_invaders_player_bullet.png")
playerX = 370
playerY = 480
playerX_change = 0
# Bullet
playerBulletX = 0
playerBulletY = 480
playerBulletX_change = 0
playerBulletY_change = 7
# ready - bullet not on the screen
# fire - bullet on the screen
playerBulletState = "ready"
bullet_sound = mixer.Sound(r"SOUND\laser.wav")
explosion_sound = mixer.Sound(r"SOUND\explosion.wav")


def player(x, y):
    screen.blit(playerImg, (x, y))


def player_fire(x, y):
    global playerBulletState
    playerBulletState = "fire"
    screen.blit(playerBulletImg, (x + 16,
                                  y + 10))  # TODO change magic numbers to proper values acording to size of player spaceship image (x + center of image, y + above image)


# Alien
alienImg = []
alienX = []
alienY = []
alienX_change = []
alienY_change = []
num_of_aliens = 6

for i in range(num_of_aliens):
    alienImg.append(pygame.image.load(r"IMG\space_invaders_alien1.png"))
    alienX.append(random.randint(0, pygame.display.get_surface().get_width() - alienImg[i].get_width()))
    alienY.append(random.randint(20, 150))
    alienX_change.append(2)
    alienY_change.append(15)


def alien(x, y, i):
    screen.blit(alienImg[i], (x, y))


def is_colision(alienX, alienY, bulletX, bulletY):
    distance = math.sqrt((math.pow(alienX - bulletX, 2)) + (math.pow(alienY - bulletY, 2)))
    if distance < 27:
        return True
    else:
        return False


# Game Loop
running = True
while running:
    # RGB (tuple needed)
    screen.fill((25, 25, 50))
    screen.blit(background, (0, 0))

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

        # if keystroke is pressed check whether its right or left
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                # logging.debug("Key pressed: {}".format(event.key))
                playerX_change = -3
            if event.key == pygame.K_RIGHT:
                # logging.debug("Key pressed: {}".format(event.key))
                playerX_change = 3
            if event.key == pygame.K_SPACE:
                if playerBulletState is "ready":
                    # logging.debug("Key pressed: {}".format(event.key))
                    playerBulletX = playerX
                    player_fire(playerBulletX, playerY)
                    bullet_sound.play()

        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT and playerX_change < 0:
                # logging.debug("Key released: {}".format(event.key))
                playerX_change = 0
            elif event.key == pygame.K_RIGHT and playerX_change > 0:
                # logging.debug("Key released: {}".format(event.key))
                playerX_change = 0

    playerX += playerX_change
    if playerX <= 0:
        playerX = 0
    if playerX >= (pygame.display.get_surface().get_width() - playerImg.get_width()):
        playerX = pygame.display.get_surface().get_width() - playerImg.get_width()

    for i in range(num_of_aliens):
        # Game Over
        if alienY[i] > 440:
            for j in range(num_of_aliens):
                alienY[j] = 2000  # TODO Create proper way to remove alien ships and check collision
            game_over_text(game_over_text_x, game_over_text_y)
            break

        alienX[i] += alienX_change[i]
        if alienX[i] <= 0:
            alienX_change[i] *= -1
            alienY[i] += alienY_change[i]
        if alienX[i] >= (pygame.display.get_surface().get_width() - playerImg.get_width()):
            alienX_change[i] *= -1
            alienY[i] += alienY_change[i]
        # Collision
        collision = is_colision(alienX[i], alienY[i], playerBulletX, playerBulletY)
        if collision:
            playerBulletY = 480
            playerBulletState = "ready"
            score_value += 1
            alienX[i] = random.randint(0, pygame.display.get_surface().get_width() - alienImg[i].get_width())
            alienY[i] = random.randint(20, 150)
            explosion_sound.play()
        alien(alienX[i], alienY[i], i)

    # Bullet Movement

    if playerBulletY <= 0:
        playerBulletY = 480
        playerBulletState = "ready"
    if playerBulletState is "fire":
        player_fire(playerBulletX, playerBulletY)
        playerBulletY -= playerBulletY_change

    player(playerX, playerY)
    show_score(score_x, score_y)
    pygame.display.update()
